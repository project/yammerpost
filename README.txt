Welcome to the Yammer Post
-----------------------------------

1) Introduction - This module helps in posting data to Yammer from your drupal site. Actualy this creates a bridge between your drupal 7 content type to a Yammer group.
Any nodes you create in your configured content type will go to Yammer. This leverages REST API's of yammer and is based OAUTH 2.0.

2)Installation steps - 

i) Install and enable the module.
					  
ii) Create an APP in from your yammer account. Give all the necessary info in the "basic info" tab. Remember to put the site url in the "Redirect URI" field in the app.

iii) Enter the client ID and client secret provided by your app in the "settings" tab of the Yammer Post module configurations.

iv) Provide the proxy server and port number along with your proxy password.

v) After you have finished , move to the "configurations" tab and provide the yammer group id and select the Content type you want to map.

v) Now any content you create in your mapped content type will be posted in the corresponding group. But remeber only your body field will be posted to yammer and not the whole node.
					  